const inventory = require('./inventry');
const car = require('./Problem-1');
let expected_answer = [{ id: 33, car_make: 'Jeep', car_model: 'Wrangler', car_year: 2011 }];
test("Car 33 is", () => {
    expect(car(inventory)).toStrictEqual(expected_answer);
})