const car = require('./Problem-5');
const inventory = require('./inventry');

const expected_answer = [
    1983, 1990, 1995, 1987, 1996,
    1997, 1999, 1987, 1995, 1994,
    1985, 1997, 1992, 1993, 1964,
    1999, 1991, 1997, 1992, 1998,
    1965, 1996, 1995, 1996, 1999
  ];

test("Length of cars older than 2000:",()=>{
    expect(car(inventory)).toStrictEqual(expected_answer);
})